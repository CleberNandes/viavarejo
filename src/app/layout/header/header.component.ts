import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'via-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isShown = false;

  constructor() { }

  toggle() {
    this.isShown = !this.isShown;
  }

  ngOnInit() {
  }

}
