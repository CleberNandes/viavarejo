import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from './template.component';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: TemplateComponent,
    data: { title: 'Home Viavarejo' }
  }
];
@NgModule({
  // name	String	F-A Icons	No
  // size	String	lg, 2x, 3x, 4x, 5x	Yes
  // fixed	Boolean	true | false	Yes
  // animation	String	spin | pulse	Yes
  // rotate	Number | String	90 | 180 | 270 horizontal | vertical	Yes
  // inverse	Boolean	true | false
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    TemplateComponent,
    HeaderComponent
  ]
})
export class TemplateModule { }
